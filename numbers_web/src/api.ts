export const API_BASE_URL = import.meta.env.API_BASE_URL ? `${import.meta.env.API_BASE_URL}/api` : "http://localhost:8000/api"

export interface IConversion {
    number: number
}

export interface IConversionResult {
    status: "ok" | "fail"
    number: string
}

const validateNum = (num: number) => {
    if (num < -1e11) {
        throw new Error("Number cannot be below -100000000000")
    }

    if (num > 1e11) {
        throw new Error("Number cannot exceed 100000000000")
    }
}

export const getNumberToEnglish = async (num: number) => {
    try {
        validateNum(num)
    } catch (err) {
        throw err;
    }

    const response = await fetch(
        `${API_BASE_URL}/num_to_english/?number=${num}`,
        {
            method: "GET",
        }
    )

    const payload = await response.json();

    if (!response.ok) {
        if (payload.detail) {
            throw new Error(payload.detail[0].msg);
        }
    }

    return payload as IConversionResult;
}


export const postNumberToEnglish = async (data: IConversion) => {
    try {
        validateNum(data.number)
    } catch (err) {
        throw err;
    }

    const response = await fetch(
        `${API_BASE_URL}/num_to_english/`,
        {
            method: "POST",
            body: JSON.stringify(data)
        }
    )

    const payload = await response.json();

    if (!response.ok) {
        if (payload.detail) {
            throw new Error(payload.detail[0].msg);
        }
    }

    return payload as IConversionResult;
}