def convert_number_to_english(number: int | str) -> str:
    """
    Converts a number (integer or string) to its equivalent long english form. Raises an exception if it cannot.

    For example, the integer number 123 becomes the string "one hundred twenty three".

    Usage:
        >>> n = 123
        >>> s = convert_number_to_english(n)
        >>> print(s)
        one hundred twenty three
    """
    if not isinstance(number, int):
        number = int(number)

    # Raise an exception if number is out of bounds
    if not (-1e11 <= number <= 1e11):
        raise ValueError(f"{number} is not within -1e11 and 1e11")

    if number == 0:
        return "zero"

    ones = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    teens = [
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen",
    ]
    tens = [
        "",
        "",
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety",
    ]
    thousands = ["", "thousand", "million", "billion"]

    def helper(num):
        if num == 0:
            return []
        elif num < 10:
            return [ones[num]]
        elif num < 20:
            return [teens[num - 10]]
        elif num < 100:
            return [tens[num // 10]] + helper(num % 10)
        elif num < 1000:
            return [ones[num // 100]] + ["hundred"] + helper(num % 100)
        else:
            for idx, unit in enumerate(thousands):
                if num < 1000 ** (idx + 1):
                    return helper(num // 1000**idx) + [unit] + helper(num % 1000**idx)

    words = [word for word in helper(abs(number)) if word]

    if number < 0:
        words.insert(0, "negative")

    return " ".join(words)
