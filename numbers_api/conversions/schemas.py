from typing import Literal, Optional
from ninja import Schema


class Conversion(Schema):
    number: int


class ConversionResult(Schema):
    status: Literal["ok", "fail"]
    number: Optional[str] = None
