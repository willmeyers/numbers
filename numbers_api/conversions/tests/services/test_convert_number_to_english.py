from django.test import TestCase

from conversions.services import convert_number_to_english


class ConvertNumberToEnglishTests(TestCase):
    def setUp(self) -> None:
        self.test_cases = [
            ("1", "one"),
            (-1, "negative one"),
            (0, "zero"),
            (1, "one"),
            (10, "ten"),
            (1e2, "one hundred"),
            (1e3, "one thousand"),
            (1e4, "ten thousand"),
            (1e5, "one hundred thousand"),
            (1e6, "one million"),
            (1e7, "ten million"),
            (1e8, "one hundred million"),
            (1e9, "one billion"),
            (123, "one hundred twenty three"),
            (123_456, "one hundred twenty three thousand four hundred fifty six"),
            (
                123_456_789,
                "one hundred twenty three million four hundred fifty six thousand seven hundred eighty nine",
            ),
        ]

    def test_convert_number_to_english(self):
        for case, expected in self.test_cases:
            converted_case = convert_number_to_english(number=case)
            self.assertEqual(converted_case, expected)

        # Test some cases that we know will raise an exception
        with self.assertRaises(ValueError):
            convert_number_to_english(number=-1e12)

        with self.assertRaises(ValueError):
            convert_number_to_english(number=1e12)

        with self.assertRaises(ValueError):
            convert_number_to_english(number="0.1")
