from unittest.mock import patch
from django.test import TestCase, Client

from numbers_api.shortcuts import reverse, reverse_querystring


class NumToEnglishAPITests(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_get_num_to_english(self):
        number = "123"

        response = self.client.get(
            reverse_querystring(
                "api-1.0.0:get_number_to_english", query_kwargs={"number": number}
            )
        )
        self.assertEqual(response.status_code, 200)

        result = response.json()
        self.assertEqual(result.get("status"), "ok")
        self.assertEqual(result.get("number"), "one hundred twenty three")

    def test_post_num_to_english(self):
        number = "123"

        # Mock asyncio sleep so that we don't have to wait when running tests
        with patch("asyncio.sleep", return_value=None):
            response = self.client.post(
                reverse("api-1.0.0:get_number_to_english"),
                data={"number": number},
                content_type="application/json",
            )

        self.assertEqual(response.status_code, 200)

        result = response.json()
        self.assertEqual(result.get("status"), "ok")
        self.assertEqual(result.get("number"), "one hundred twenty three")
