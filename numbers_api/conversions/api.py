import asyncio

from django.http import HttpRequest
from ninja import Router
from ninja.errors import ValidationError

from numbers_api.schemas import ErrorResponse
from conversions.schemas import Conversion, ConversionResult
from conversions.services import convert_number_to_english


conversions_router = Router()


@conversions_router.get(
    "num_to_english/", response={200: ConversionResult, 422: ErrorResponse}
)
async def get_number_to_english(request: HttpRequest, number: str) -> ConversionResult:
    try:
        number_str = convert_number_to_english(number=number)
    except ValueError as exc:
        raise ValidationError(errors=[{"msg": str(exc)}])

    response = ConversionResult(status="ok", number=number_str)

    return response


@conversions_router.post(
    "num_to_english/", response={200: ConversionResult, 422: ErrorResponse}
)
async def post_number_to_english(
    request: HttpRequest, data: Conversion
) -> ConversionResult:
    number = data.number

    try:
        number_str = convert_number_to_english(number=number)
    except ValueError as exc:
        raise ValidationError(errors=[{"msg": str(exc)}])

    response = ConversionResult(status="ok", number=number_str)

    # Simulate a long-running task
    await asyncio.sleep(5)

    return response
