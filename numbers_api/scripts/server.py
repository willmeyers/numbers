import os

import uvicorn


def main():
    environment = os.getenv("ENVIRONMENT", "development")
    num_workers = int(os.getenv("NUM_WORKERS", "3"))
    if environment == "production":
        uvicorn.run(
            "numbers_api.asgi:application",
            host="0.0.0.0",
            port=8000,
            log_level="info",
            workers=num_workers,
        )
    else:
        uvicorn.run(
            "numbers_api.asgi:application",
            host="0.0.0.0",
            port=8000,
            log_level="debug",
            reload=True,
        )


if __name__ == "__main__":
    main()
