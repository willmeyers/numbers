from pydantic import BaseModel
from typing import Any, List, Dict, Literal


class ErrorResponse(BaseModel):
    status: Literal["fail"]
    detail: List[Dict[Any, Any]]
