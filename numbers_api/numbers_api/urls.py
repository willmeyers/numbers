"""
URL configuration for numbers_api project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.http import HttpRequest
from django.contrib import admin
from django.urls import path
from ninja import NinjaAPI, Router
from ninja.errors import ValidationError

from numbers_api.views import healthcheck_view
from numbers_api.schemas import ErrorResponse
from conversions.api import conversions_router


api = NinjaAPI(
    title="Numbers API", description="Convert numbers to their english equivalents."
)

api_router = Router()


@api.exception_handler(ValidationError)
def custom_exception_handler(request, exc):
    """
    Custom exception handler for validation errors thrown that appends a `status` of "fail" to the final response.
    """
    response = ErrorResponse(detail=exc.errors, status="fail")

    return api.create_response(
        request=request,
        data=response.model_dump(),
        status=422,
    )


# Router routes
api_router.add_router("/", conversions_router)


api.add_router("", api_router)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("health/", healthcheck_view),
    path("api/", api.urls),
]
