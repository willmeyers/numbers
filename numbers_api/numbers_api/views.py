from django.http import HttpRequest, JsonResponse


def healthcheck_view(request: HttpRequest):
    return JsonResponse({"status": "ok"})
