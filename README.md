# Numbers to English

A fullstack application demo.

## Getting up and Running

Below are instructions on how to run both the API and SPA locally. To get stated, clone this repo.

### From Docker

If you have docker, simply take advantage of the existing `docker-compose` file. Run:

```bash
docker-compose up
```

Navigate to [http://localhost:80](http://localhost/) on completetion.

### From Source

#### Backend API

The backend API serivce is located in the `numbers_api` directory.

We use Django and [Django Ninja](https://django-ninja.dev/) in combination with [Poetry](https://python-poetry.org/) to handle dependencies.

First run:

```bash
poetry install --with dev
```

Then spin up a developmnent server with:

```bash
poetry run runserver
```

We use [django-environ](https://django-environ.readthedocs.io/en/latest/) to help manage environment variables. You may modify the environment in `numbers_api/settings.py`. 

#### Frontend SPA

The frontend SPA is located in the `numbers_web` directory.

We use the [Bun runtime](https://bun.sh/) with [Vue.js](https://vuejs.org/).

First run:

```bash
bun install
```

Then spin up a development server with

```bash
bun run dev
```

**⚠️ Important**
> Ensure you've correctly set the environment varaible, `API_BASE_URL`, to match your local API's listening host and port.
> For example, 
> ```bash
> export API_BASE_URL="http://0.0.0.0:8000"
> ```
